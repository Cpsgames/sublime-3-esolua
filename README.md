ESOLua
========

A Sublime 3 language plugin for The Elder Scrolls Online's lua syntax.

How To Install
========

**Using Package Control**

The easiest way to install is to use [Sublime Package Control](http://wbond.net/sublime_packages/package_control).


**Manual Install**

1. Open your packages folder by clicking "Preferences" and then "Browse Packages"
2. Download and extract into the packages folder

You should then be good to go!

-Or-

Clone the repo!